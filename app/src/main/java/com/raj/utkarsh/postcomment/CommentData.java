package com.raj.utkarsh.postcomment;

/**
 * Created by win 7 on 4/21/2018.
 */


public class CommentData {

    int imageView;
    String userName,comment;

    public CommentData(int imageView,String userName,String comment) {
        this.imageView = imageView;
        this.userName = userName;
        this.comment = comment;
    }

    public int getImageView() {
        return imageView;
    }

    public String getUserName() {
        return userName;
    }

    public String getComment() {
        return comment;
    }

}
