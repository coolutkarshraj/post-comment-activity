package com.raj.utkarsh.postcomment;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomListView extends ArrayAdapter<CommentData> {
    private final Activity context;
    private ArrayList<CommentData> commentData;

    public CustomListView(Activity context,
                          ArrayList<CommentData> commentData) {
        super(context,R.layout.comment_layout,commentData);
        this.context = context;
        this.commentData = commentData;

    }

    @Override
    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.comment_layout,null,true);
        CircleImageView profileImage = (CircleImageView) rowView.findViewById(R.id.profile_image);
        TextView userName = (TextView) rowView.findViewById(R.id.user_name);
        TextView comment = (TextView) rowView.findViewById(R.id.comment);

        CommentData commentData = this.commentData.get(position);
        profileImage.setImageResource(commentData.getImageView());
        userName.setText(commentData.getUserName());
        comment.setText(commentData.getComment());
        return rowView;
    }
}