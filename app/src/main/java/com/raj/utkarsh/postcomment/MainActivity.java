package com.raj.utkarsh.postcomment;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    LinearLayout ll_post_view;
    View inflatedView;
    PopupWindow popWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        initListner();
    }

    private void initListner() {
        ll_post_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onShowPopup(v);
            }
        });
    }

    private void init() {
        ll_post_view = findViewById(R.id.ll_post_view);
    }

    public void onShowPopup(View v) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflatedView = layoutInflater.inflate(R.layout.popup_layout,null,false);
        ListView listView = (ListView) inflatedView.findViewById(R.id.commentsListView);
        LinearLayout headerView = (LinearLayout) inflatedView.findViewById(R.id.ll_toplayout);
        Display display = getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        setSimpleList(listView);
        popWindow = new PopupWindow(inflatedView,width,height - 50,true);
        popWindow.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_bg));
        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popWindow.setAnimationStyle(R.style.PopupAnimation);
        popWindow.showAtLocation(v,Gravity.BOTTOM,0,100);
    }

    void setSimpleList(ListView listView) {

        final ArrayList<CommentData> commentData = new ArrayList<>();
        commentData.add(new CommentData(R.drawable.person_first_icon,"utkarsh","Sooo cuteee"));
        commentData.add(new CommentData(R.drawable.person_second_image,"pratima","Looking Gorgeous"));
        commentData.add(new CommentData(R.drawable.person_third_image,"sandeep","Nice Pic Bro"));
        commentData.add(new CommentData(R.drawable.person_third_image,"kuldeep","Where are you bro"));
        commentData.add(new CommentData(R.drawable.person_first_icon,"aryan","how are you"));
        commentData.add(new CommentData(R.drawable.person_second_image,"Aarti","Looking Gorgeous"));
        commentData.add(new CommentData(R.drawable.person_third_image,"shivank","Nice Pic Bro"));
        commentData.add(new CommentData(R.drawable.person_third_image,"utkarsh","Nice Pic Bro"));
        commentData.add(new CommentData(R.drawable.person_first_icon,"shivank","how are you"));
        commentData.add(new CommentData(R.drawable.person_second_image,"pratima","Looking Gorgeous"));
        commentData.add(new CommentData(R.drawable.person_third_image,"shivank","Nice Pic Bro"));
        commentData.add(new CommentData(R.drawable.person_third_image,"sandeep","Nice Pic Bro"));
        commentData.add(new CommentData(R.drawable.person_first_icon,"shivank","Nice Pic Bro"));
        commentData.add(new CommentData(R.drawable.person_second_image,"pratima","how are you"));
        commentData.add(new CommentData(R.drawable.person_third_image,"kuldeep","how are you"));
        commentData.add(new CommentData(R.drawable.person_third_image,"sandeep","Nice Pic Bro"));
        CustomListView adapter = new CustomListView(MainActivity.this,commentData);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent,View view,
                                    int position,long id) {
                Toast.makeText(MainActivity.this,"You Clicked at " + commentData.get(position).getComment(),Toast.LENGTH_SHORT).show();

            }
        });

    }
}
